//
//  SoeInit.m
//  SoupExt
//
//  Created by Dan on 26.10.2021.
//

#import "SoeInit.h"

@implementation SoeInit

+ (void)initialize {
    (void)NseInit.class;
    (void)TlsInit.class;
    (void)GneInit.class;
    
    soe_init();
    soe_init_i18n((gchar *)NSBundle.mainBundle.bundlePath.UTF8String);
}

@end
